from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return HttpResponse("Nathanael Kevin Sitepu - 11th Grade.")
def index(request):
    return HttpResponse("Hello")
def identity(request, name):
    return render(request, "home.html", context={'name': name})
def test(request):
    return HttpResponse('Nathanael Kevin Sitepu - 11th Grade')
def check(request):
    req = request.GET
    number = int(req["number"])
    if number % 2 == 0:
        text = str(number) + " is even"
    else:
        text = str(number) + " is odd"
    return HttpResponse(text)
    #sir idk why but if i just do return HttpResponse({number}, " is even/odd") the web bg turns black so i am now using a roundabout way